export const player = {};

var currentTimeInterval = null;

player.url = "";

player.config = {
  top: 0,
  left: 0,
  width: 1920,
  height: 1080
};

player.showStreamLoaderActive = false;

player.setUrl = function(streamUrl) {
  player.url = streamUrl;
};

player.setDisplayConfig = function(playerConfig) {
  player.config = playerConfig;
};

player.setDisplayView = function(playerConfig) {
  player.config = playerConfig;

  // // // webapis.avplay.setDisplayRect(
  // // //   playerConfig.left,
  // // //   playerConfig.top,
  // // //   playerConfig.width,
  // // //   playerConfig.height
  // // // );
};

player.play = function() {
  // // // if (webapis.avplay.getState() != "PLAYING") {
  // // //   if (webapis.avplay.getState() != "PAUSED") {
  // // //     webapis.avplay.open(player.url);
  // // //     webapis.avplay.setDisplayRect(
  // // //       player.config.left,
  // // //       player.config.top,
  // // //       player.config.width,
  // // //       player.config.height
  // // //     );
  // // //     webapis.avplay.setListener(getEventListeners());
  // // //     webapis.avplay.prepareAsync(
  // // //       function() {
  // // //         console.log("PLAYER PREPARE SUCCESS");
  // // //         webapis.avplay.play();
  // // //       },
  // // //       function() {
  // // //         console.log("PLAYER PREPARE ERROR");
  // // //       }
  // // //     );
  // // //   } else {
  // // //     webapis.avplay.play();
  // // //   }
  // // // }
};

player.seekPlay = function(time) {
  // // // if (webapis.avplay.getState() != "PLAYING") {
  // // //   if (webapis.avplay.getState() != "PAUSED") {
  // // //     webapis.avplay.open(player.url);
  // // //     webapis.avplay.setDisplayRect(
  // // //       player.config.left,
  // // //       player.config.top,
  // // //       player.config.width,
  // // //       player.config.height
  // // //     );
  // // //     webapis.avplay.setListener(getEventListeners());
  // // //     webapis.avplay.prepareAsync(
  // // //       function() {
  // // //         console.log("PLAYER PREPARE SUCCESS");
  // // //         player.seekTo(time);
  // // //       },
  // // //       function() {
  // // //         console.log("PLAYER PREPARE ERROR");
  // // //       }
  // // //     );
  // // //   } else {
  // // //     webapis.avplay.play();
  // // //   }
  // // // }
};

player.pause = function() {
  // // // if (webapis.avplay.getState() === "PLAYING") webapis.avplay.pause();
};

player.stop = function() {
  // // // if (
  // // //   webapis.avplay.getState() === "PLAYING" ||
  // // //   webapis.avplay.getState() === "PAUSED"
  // // // )
  // // //   webapis.avplay.stop();
};

player.close = function() {
  // // // webapis.avplay.close();
};

player.forward = function(time) {
  // // // if (
  // // //   webapis.avplay.getState() === "PLAYING" ||
  // // //   webapis.avplay.getState() === "PAUSED"
  // // // ) {
  // // //   webapis.avplay.jumpForward(
  // // //     time,
  // // //     function() {
  // // //       console.log("JUMP FORWARD SUCCESS");
  // // //       player.play();
  // // //     },
  // // //     function() {
  // // //       console.log("JUMP FORWARD ERROR");
  // // //     }
  // // //   );
  // // // }
};

player.backward = function(time) {
  // // // if (
  // // //   webapis.avplay.getState() === "PLAYING" ||
  // // //   webapis.avplay.getState() === "PAUSED"
  // // // )
  // // //   webapis.avplay.jumpBackward(time);
};

player.seekTo = function(time) {
  // if (
  //   webapis.avplay.getState() === "PLAYING" ||
  //   webapis.avplay.getState() === "PAUSED"
  // ) {
  //   webapis.avplay.seekTo(
  //     time,
  //     function() {
  //       console.log("SEEK SUCCESS");
  //       // var current_time = 0;

  //       // currentTimeInterval = setInterval(function() {
  //       //   current_time = player.getCurrentTime();

  //       //   if (current_time >= time) {
  //       //     player.play();
  //       //     clearInterval(currentTimeInterval);
  //       //   }
  //       // }, 1000);
  //       player.play();
  //     },
  //     function() {
  //       console.log("SEEK ERROR");
  //     }
  //   );
  // }

  // // // webapis.avplay.seekTo(
  // // //   time,
  // // //   function() {
  // // //     console.log("SEEK SUCCESS");
  // // //     // var current_time = 0;

  // // //     // currentTimeInterval = setInterval(function() {
  // // //     //   current_time = player.getCurrentTime();

  // // //     //   if (current_time >= time) {
  // // //     //     player.play();
  // // //     //     clearInterval(currentTimeInterval);
  // // //     //   }
  // // //     // }, 1000);

  // // //     if ( webapis.avplay.getState() != "PLAYING" && webapis.avplay.getState() != "PAUSED" ) {
  // // //       webapis.avplay.play();
  // // //     } else {
  // // //         player.play();
  // // //     }

  // // //   },
  // // //   function() {
  // // //     console.log("SEEK ERROR");
  // // //   }
  // // // );

}

player.getDuration = function() {
  // if (
  //   webapis.avplay.getState() === "PLAYING" ||
  //   webapis.avplay.getState() === "PAUSED"
  // )
  // // // return webapis.avplay.getDuration();
};

player.getCurrentTime = function() {
  // // // if (
  // // //   webapis.avplay.getState() === "PLAYING" ||
  // // //   webapis.avplay.getState() === "PAUSED"
  // // // )
  // // //   return webapis.avplay.getCurrentTime();
  // // // else return 0;
};

player.getPlayerStatus = function() {
  // // // return webapis.avplay.getState();
};

player.showStreamLoader = () => {
  //console.log("Show loader stream TOP");
  //var buffer_amination = document.getElementById("buffer_amination");

  // // // if (buffer_amination != null) {
  // // //   buffer_amination.classList.add("active_loading_buffer");
  // // //   player.showStreamLoaderActive = true;
  // // // }

  //console.log("Show loader stream BOTTOM");
};

player.hideStreamLoader = () => {
  // if (player.showStreamLoaderActive) {
  //   var buffer_amination = document.getElementById("buffer_amination");

  //   if (buffer_amination != null) {
  //     buffer_amination.classList.remove("active_loading_buffer");
  //     document.getElementById("buffer_progres_percent").innerHTML = 0;
  //     player.showStreamLoaderActive = false;
  //   }
  // }
};

player.getStreamInfo = () => {
  // // // return webapis.avplay.getCurrentStreamInfo();
}

function getEventListeners() {
  // // // var listeners = {
  // // //   onbufferingstart: function() {
  // // //     //console.log('Buffering start.');
  // // //   },

  // // //   onbufferingprogress: function(percent) {
  // // //     document.getElementById("buffer_progres_percent").innerHTML = percent;
  // // //   },

  // // //   onbufferingcomplete: function() {
  // // //     //console.log('Buffering complete.');

  // // //     //document.getElementById("buffer_progres_percent").innerHTML = "";

  // // //     console.log(
  // // //       "Current time in onbufferingcomplete listener: ",
  // // //       player.getCurrentTime()
  // // //     );

  // // //     let streamInfo = player.getStreamInfo();

  // // //     console.log("Stream Info: ", streamInfo);

  // // //     var TrackInfo = webapis.avplay.getTotalTrackInfo();
  // // //     console.log("Track Info: ", TrackInfo);

  // // //     //webapis.avplay.setSelectTrack('TEXT',5);
  // // //     //webapis.avplay.setSelectTrack('AUDIO',2);


  // // //     // setInterval(function(){
  // // //     //   console.log("Current time in onbufferingcomplete listener: ", player.getCurrentTime());
  // // //     // }, 1000)
  // // //   },
  // // //   onstreamcompleted: function() {
  // // //     console.log("Stream Completed");
  // // //   },

  // // //   oncurrentplaytime: function(currentTime) {
  // // //     //console.log('Current playtime: ' + currentTime);
  // // //     player.hideStreamLoader();
  // // //   },

  // // //   onerror: function(eventType) {
  // // //     console.log("event type error : " + eventType);
  // // //     player.hideStreamLoader();
  // // //   },

  // // //   onevent: function(eventType, eventData) {
  // // //     console.log("event type: " + eventType + ", data: " + eventData);
  // // //   },

  // // //   onsubtitlechange: function(duration, text, data3, data4) {
  // // //     // console.log('subtitleText: ' + text);
  // // //   },
  // // //   ondrmevent: function(drmEvent, drmData) {
  // // //     // console.log('DRM callback: ' + drmEvent + ', data: ' + drmData);
  // // //   }
  // // // };

  // // // return listeners;
}
