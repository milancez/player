
class Utility {
  keyCodes = {
    left: 37,
    up: 38,
    right: 39,
    down: 40,
    enter: 13,
    back: 10009,
    exit: 10182,
    play: 415,
    pause: 19,
    stop: 413,
    red: 403,
    green: 404,
    yellow: 405,
    blue: 406,
    smartPlayPause: 10252,
    channelUp: 427,
    channelDown: 428,
    done: 65376,
    cancel: 65385,
    backward: 412,
    forward: 417,
    record: 416,
    0: 48,
    1: 49,
    2: 50,
    3: 51,
    4: 52,
    5: 53,
    6: 54,
    7: 55,
    8: 56,
    9: 57,
    esc: 27
  }

  // Remove keydown handler
  removeKeyHandler = handler => {
    window.removeEventListener("keydown", handler);
  }

  // Add keydown handler
  addKeyHandler = handler => {
    window.addEventListener("keydown", handler);
  }

}




export default new Utility;