import { combineReducers } from "redux";

function keyHandler(state = "", action) {
  switch (action.type) {
    case "SET_KEY_HANDLER":
      return action.handler;
    default:
      return state;
  }
}

function prevKeyHandler(state = "Home", action) {
  switch (action.type) {
    case "SET_PREVIOUS_KEY_HANDLER":
      return action.handler;
    default:
      return state;
  }
}

function activeCarousel(state = 0, action) {
  switch (action.type) {
    case "SET_ACTIVE_CAROUSEL":
      return action.value;
    default:
      return state;
  }
}

function containerPosition(state = 330, action) {
  switch (action.type) {
    case "SET_CONTAINER_POSITION":
      return action.value;
    default:
      return state;
  }
}

function playerFullScreen(state = false, action) {
  switch (action.type) {
    case "SET_PLAYER_FULLSCREEN":
      return action.status;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
    keyHandler,
    prevKeyHandler,
    activeCarousel,
    containerPosition,
    playerFullScreen
});

export default rootReducer;