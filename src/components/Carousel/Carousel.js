import React, { Component } from "react";
import { connect } from "react-redux";
import Utility from "../../helpers/Utility";
import { setKeyHandler, setPrevKeyHandler, setActiveCarousel, setContainerPosition, setPlayerFullScreen } from "../../actions/actions";

var offset = 470;

class Carousel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
      startIndex: 0,
      endIndex: 7
    };
  }

  componentDidMount() {
    console.log("Category: ", this.props.category);
    if (this.props.id === 0) {
        Utility.addKeyHandler(this.keyHandler);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // if (this.props.keyHandler === "Carousel" && this.props.activeCarousel === this.props.id) {
    //   Utility.addKeyHandler(this.keyHandler);
    // } else {
    //     Utility.removeKeyHandler(this.keyHandler);
    // }
      
    
  }

  left = () => {
    console.log("LEFT");
    if (this.state.currentIndex > 0) {
        this.setState({
            currentIndex: this.state.currentIndex - 1,
            startIndex: this.state.startIndex - 1,
            endIndex: this.state.endIndex - 1
        })
    }
  };

  right = () => {
    console.log("RIGHT");
    if (this.state.currentIndex < this.props.category.movies.length - 1) {
        this.setState({
            currentIndex: this.state.currentIndex + 1,
            startIndex: this.state.startIndex + 1,
            endIndex: this.state.endIndex + 1
        })
    }
  };

  up = () => {
    console.log("UP");
    if (this.props.activeCarousel > 0) {
        this.props.setActiveCarousel(this.props.activeCarousel - 1);
        this.props.setContainerPosition(this.props.containerPosition + offset);
    }
  };

  down = () => {
    console.log("DOWN");
    if (this.props.activeCarousel < this.props.categoriesLength - 1) {
        this.props.setActiveCarousel(this.props.activeCarousel + 1);
        this.props.setContainerPosition(this.props.containerPosition - offset);
    }
  };

  enter = () => {
    this.props.setPlayerFullScreen(true);
    this.props.setPrevKeyHandler("Carousel");
    this.props.setKeyHandler("PlayerFullScreen");
  }

  keyHandler = event => {
    switch (event.keyCode) {
      case Utility.keyCodes.left:
        this.left();
        break;
      case Utility.keyCodes.right:
        this.right();
        break;
      case Utility.keyCodes.up:
        this.up();
        break;
      case Utility.keyCodes.down:
        this.down();
        break;
      case Utility.keyCodes.enter:
        this.enter();
        break;

      case Utility.keyCodes.back:
        break;

      case Utility.keyCodes.esc:
        break;

      case Utility.keyCodes.exit:
        break;

      default:
        break;
    }
  };

  render() {
    if (this.props.keyHandler === "Carousel" && this.props.activeCarousel === this.props.id) {
      Utility.addKeyHandler(this.keyHandler);
    } else {
        Utility.removeKeyHandler(this.keyHandler);
    }


    return (
      <div className="category_box">
        <div className="category_title">{this.props.category.name}</div>
        <div className="carousel">
          {this.props.category.movies
            .slice(this.state.startIndex, this.state.endIndex)
            .map((movie, index) => (
              <div
                key={index}
                className={
                  "movie_item" +
                  (this.props.id === this.props.activeCarousel && index === 0
                    ? " active"
                    : "")
                }
              >
                <div className="movie_poster">
                  <img src={movie.stream_icon} />
                </div>
                {this.props.id === this.props.activeCarousel && index === 0 ? (
                  <div className="movie_details">
                    <div className="movie_details_title">{movie.title}</div>
                    <div className="movie_description">
                      {movie.details.overview}
                    </div>
                  </div>
                ) : (
                  <div className="movie_title">{movie.title}</div>
                )}
              </div>
            ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {
  setKeyHandler,
  setPrevKeyHandler,
  setActiveCarousel,
  setContainerPosition,
  setPlayerFullScreen
})(Carousel);
