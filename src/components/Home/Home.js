import React, { Component } from 'react';
import { connect } from "react-redux";
import axios from "axios";
import Carousel from "../Carousel/Carousel";
import { setKeyHandler, setPrevKeyHandler } from "../../actions/actions";

class Home extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          categories: []
        }
    }

    componentDidMount() {
        console.log("Home");
        //movie-categories

        this.getCategories();
        this.props.setKeyHandler("Carousel");
    }

    getCategories = () => {
        let token = localStorage.getItem("token");
        let url = "https://dev.bridgetv.mysafeservers.com/api/" + "movie-categories";
        let headers = {
            "Content-Type": "aplication/json",
            Authorization: "Bearer " + token
        };

        axios
            .get(url, { headers })
            .then(res => {
                console.log("Categories response", res.data);
                this.setState({
                    categories: res.data
                })
            })
            .catch(function(error) {
                // handle error
                console.log("ERRORRR CATEGORIES", error);
            });
    }

    render() { 
        return ( 
            <div className="home">
                <div 
                    className="slider_list" 
                    style={{
                        transform: `translate(0, ${this.props.containerPosition}px)`
                    }}
                >
                {
                    this.state.categories.length > 0 ?
                        this.state.categories.map((cat, index) => (
                            <Carousel 
                                key={index} 
                                category={cat} 
                                id={index}
                                categoriesLength={this.state.categories.length}
                            />
                        )) 
                    : ""
                }
                </div>
                
            </div> 
        );
    }
}
 
function mapStateToProps(state) {
    return state;
  }
  
export default connect(mapStateToProps, {
    setKeyHandler,
    setPrevKeyHandler
})(Home);

