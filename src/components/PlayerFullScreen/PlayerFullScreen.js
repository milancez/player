import React, { Component } from 'react';
import Utility from "../../helpers/Utility";
import { connect } from "react-redux";
import { player } from "../../Player/Player";
import {
    setPlayerFullScreen,
    setPrevKeyHandler,
    setKeyHandler
} from "../../actions/actions";


class PlayerFullScreen extends Component {
    constructor(props) {
        super(props)


    }

    back = () => {
        this.props.setKeyHandler(this.props.prevKeyHandler);
        this.props.setPlayerFullScreen(false);
    }

    keyHandler = event => {
        switch (event.keyCode) {
          case Utility.keyCodes.left:
            //this.left();
            break;
          case Utility.keyCodes.right:
            //this.right();
            break;
          case Utility.keyCodes.up:
            //this.up();
            break;
          case Utility.keyCodes.down:
            //this.down();
            break;
          case Utility.keyCodes.enter:
            //this.enter();
            break;
    
          case Utility.keyCodes.back:
            this.back();
            break;
    
          case Utility.keyCodes.esc:
            this.back();
            break;
    
          case Utility.keyCodes.exit:
            break;
    
          default:
            break;
        }
      };

    render() { 
        if (this.props.keyHandler === "PlayerFullScreen") {
            Utility.addKeyHandler(this.keyHandler);
          } else {
              Utility.removeKeyHandler(this.keyHandler);
          }


        return ( 
            <div className="playerFullScreen">

            </div>
         );
    }
}

function mapStateToProps(state) {
    return state;
}

export default connect(
    mapStateToProps,
    {
      setPrevKeyHandler,
      setPlayerFullScreen,
      setKeyHandler
    }
  )(PlayerFullScreen);