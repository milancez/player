export function setKeyHandler(handler) {
  const action = {
    type: "SET_KEY_HANDLER",
    handler: handler
  };
  return action;
}

export function setPrevKeyHandler(handler) {
  const action = {
    type: "SET_PREVIOUS_KEY_HANDLER",
    handler: handler
  };
  return action;
}

export function setActiveCarousel(value) {
  const action = {
    type: "SET_ACTIVE_CAROUSEL",
    value: value
  };
  return action;
}

export function setContainerPosition(value) {
  const action = {
    type: "SET_CONTAINER_POSITION",
    value: value
  };
  return action;
}

export function setPlayerFullScreen(status) {
  const action = {
    type: "SET_PLAYER_FULLSCREEN",
    status: status
  };

  return action;
}