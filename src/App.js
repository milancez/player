import React, { Component } from 'react';
import axios from "axios";
import { connect } from "react-redux";
import './App.css';
import Home from "./components/Home/Home";
import PlayerFullScreen from "./components/PlayerFullScreen/PlayerFullScreen";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authState: false
    }
  }

  componentDidMount() {
    let users = localStorage.getItem("users");

    if (users) {
      this.setState({
        authState: true
      })
    } else {
      this.login();
    }
  }


  login = () => {
    let subCode = "6198 4158 5985 9600";

    let data = {
      subscription_code: subCode,
      device: {
        mac_address: 22-22-22-22-22,
      }
    };

    //console.log("Device Info Data: ", data);

    axios({
      method: "post",
      url: "https://dev.bridgetv.mysafeservers.com/api/" + "login",
      data: data,
      headers: {
        "Content-Type": "aplication/json"
      }
    })
      .then(res => {
        console.log("Login response: ", res);

        localStorage.setItem("users", JSON.stringify(res.data))
        localStorage.setItem("token", res.data[0].token);

        this.setState({
          authState: true
        })
        
      })
      .catch(err => {
        console.log("response error", err.response);
      });
  }

  render() { 
    return ( 
      <div className="app">
        {
          this.state.authState ? <Home /> : ""
        }
        {this.props.playerFullScreen ? <PlayerFullScreen /> : ""}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {
  
})(App);
